# terraform-gitlab

**Created by Udesh Udayakumar**__

This repository contains sample code for creating an EC2 instance through GitLab pipeline using Terraform.

Pre-requisites:
1. Have your own AWS account(free-tier also possible).
2. Create a user with necessary permission for creating an EC2 instance, if not present.

Steps:
1. Clone the repository to your GitLab project.
2. Update main.tf file. 
3. Change the ACCESS KEY and SECRET KEY with your keys in the code. 
4. Commit the changes.
5. Run the CI/CD pipeline.
