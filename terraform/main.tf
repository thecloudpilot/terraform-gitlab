terraform{
    required_providers {
        aws = {
            source = "hashicorp/aws"
            version = "~> 3.0"
        }
    }    
}


provider "aws"{
    region = "us-east-1"
    access_key= #ACCESS KEY(enclosed in " ")
    secret_key= #SECRET KEY(enclosed in " ")
}

resource "aws_instance" "testinstance" {
    ami = "ami-0aeeebd8d2ab47354"
    instance_type = "t2.micro"
}
